﻿using System.Collections.Generic;
using System.Linq;
using Database.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Services
{
    public class AirportService : IAirportService
    {
        private readonly IRepository<Airport> _airportRepository;

        public AirportService(IRepository<Airport> airportRepository)
        {
            _airportRepository = airportRepository;
        }

        public List<Airport> Read()
        {
            return _airportRepository.ReadAll().ToList();
        }

        public Airport Read(int id)
        {
            return _airportRepository.Read(id);
        }
    }
}
