﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infrastructure.Interfaces;

namespace Infrastructure.Services
{
    public class Service<TEntity> : IService<TEntity> where TEntity : class
    {
        private readonly IRepository<TEntity> _repository;

        public Service(IRepository<TEntity> repository)
        {
            this._repository = repository;
        }

        public TEntity Read(int id)
        {
            return _repository.Read(id);
        }

        public void Create(TEntity data)
        {
            _repository.Create(data);
        }

        public void Update(TEntity data, int id)
        {
            _repository.Update(data, id);
        }

        public void Delete(int id)
        {
            var data = _repository.Read(id);
            if (data != null)
                _repository.Delete(data);
        }

        public List<TEntity> ReadAll()
        {
            return _repository.ReadAll().ToList();
        }
    }
}
