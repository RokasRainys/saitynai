﻿using System.Collections.Generic;
using System.Linq;
using Database.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Services
{
    public class CountryService : ICountryService
    {
        private readonly IRepository<Country> _countryRepository;

        public CountryService(IRepository<Country> countryRepository)
        {
            _countryRepository = countryRepository;
        }

        public void Update(Country country, int id)
        {
            _countryRepository.Update(country, id);
        }

        public List<Country> ReadAll()
        {
            return _countryRepository.ReadAll().ToList();
        }

        public Country Read(int id)
        {
            return _countryRepository.Read(id);
        }
    }
}
