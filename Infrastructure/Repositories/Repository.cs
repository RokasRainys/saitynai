﻿using System.Collections.Generic;
using System.Linq;
using Database;
using Infrastructure.Interfaces;

namespace Infrastructure.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly AirportDbContext _airportDbContext;

        public Repository(AirportDbContext dbContext)
        {
            _airportDbContext = dbContext;
        }
        public void Create(TEntity data)
        {
            _airportDbContext.Set<TEntity>().Add(data);
            _airportDbContext.SaveChanges();
        }

        public TEntity Read(int id)
        {
            return _airportDbContext.Set<TEntity>().Find(id);
        }

        public void Update(TEntity data, int id)
        {
            var oldData = _airportDbContext.Set<TEntity>().Find(id);
            if (oldData == null) return;
            _airportDbContext.Entry(oldData).CurrentValues.SetValues(data);
            _airportDbContext.SaveChanges();
        }

        public void Delete(TEntity data)
        {
            _airportDbContext.Set<TEntity>().Remove(data);
            _airportDbContext.SaveChanges();
        }

        public IEnumerable<TEntity> ReadAll()
        {
            return _airportDbContext.Set<TEntity>().ToList();
        }
    }
}
