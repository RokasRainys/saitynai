﻿using System.Collections.Generic;
using System.Linq;
using Database;
using Database.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Repositories
{
    public class CountryRepository : ICountryRepository
    {
        private readonly AirportDbContext _dbContext;

        public CountryRepository(AirportDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Update(Country country, int id)
        {
            var countryOld = _dbContext.Countries.Find(id);
            countryOld.Airports.Clear();
            foreach (var airport in country.Airports)
            {
                countryOld.Airports.Add(airport);
            }
            countryOld.Name = country.Name;
            countryOld.Iso = country.Iso;
            _dbContext.SaveChanges();
        }

        public List<Country> Read()
        {
            var countries = _dbContext.Set<Country>().ToList();
            foreach (var country in countries)
            {
                var airports = (from b in _dbContext.Airports
                    where b.CountryId == country.CountryId
                    select b).ToList();
                country.Airports = airports;
            }
            return countries;
        }
        public Country Read(int id)
        {
            var country = (from b in _dbContext.Countries
                where b.CountryId == id
                select b).FirstOrDefault();
            var airports = (from b in _dbContext.Airports
                where b.CountryId == id
                select b).ToList();
            if (country != null) country.Airports = airports;
            return country;
        }
    }
}
