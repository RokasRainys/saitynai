﻿using System.Collections.Generic;
using System.Linq;
using Database;
using Database.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Repositories
{
    public class AirportRepository : IAirportRepository
    {
        private readonly AirportDbContext _airportDbContext;

        public AirportRepository(AirportDbContext airportDbContext)
        {
            _airportDbContext = airportDbContext;
        }

        public List<Airport> Read()
        {
            var airports = _airportDbContext.Set<Airport>().ToList();
            foreach (var airport in airports)
            {
                airport.Country = (from b in _airportDbContext.Countries
                    where airport.CountryId == b.CountryId
                    select b).FirstOrDefault();
            }

            return airports;
        }

        public Airport Read(int id)
        {
            var airport = (from b in _airportDbContext.Airports
                where b.AirportId == id
                select b).FirstOrDefault();
            if (airport != null)
            {
                airport.Country = (from b in _airportDbContext.Countries
                    where b.CountryId == airport.CountryId
                    select b).FirstOrDefault();
            }
            return airport;
        }
    }
}
