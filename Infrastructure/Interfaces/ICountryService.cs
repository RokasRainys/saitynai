﻿using System.Collections.Generic;
using Database.Models;

namespace Infrastructure.Interfaces
{
    public interface ICountryService
    {
        void Update(Country country, int id);
        List<Country> ReadAll();
        Country Read(int id);
    }
}
