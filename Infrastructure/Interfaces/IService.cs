﻿using System.Collections.Generic;

namespace Infrastructure.Interfaces
{
    public interface IService<TEntity> where TEntity : class
    {
        TEntity Read(int id);
        void Create(TEntity data);
        void Update(TEntity data, int id);
        void Delete(int id);
        List<TEntity> ReadAll();
    }
}