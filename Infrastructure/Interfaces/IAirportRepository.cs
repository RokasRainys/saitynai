﻿using System.Collections.Generic;
using Database.Models;

namespace Infrastructure.Interfaces
{
    public interface IAirportRepository
    {
        List<Airport> Read();
        Airport Read(int id);
    }
}
