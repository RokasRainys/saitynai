﻿using System.Collections.Generic;
using Database.Models;

namespace Infrastructure.Interfaces
{
    public interface IAirportService
    {
        List<Airport> Read();
        Airport Read(int id);
    }
}
