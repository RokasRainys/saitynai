﻿using System.Collections.Generic;
using Database.Models;

namespace Infrastructure.Interfaces
{
    public interface ICountryRepository
    {
        void Update(Country country, int id);
        List<Country> Read();
        Country Read(int id);

    }
}
