﻿using Database.Models;
using Infrastructure.DTO;

namespace Infrastructure.Interfaces
{
    public interface IUserService
    {
        bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt);
        void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt);
        User AuthenticateUser(UserDto userDto);
        User GenerateJWT(User user);
    }
}
