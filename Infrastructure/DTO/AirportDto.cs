﻿using Database.Models;

namespace Infrastructure.DTO
{
    public class AirportDto
    {
        public int AirportId { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
        public virtual Country Country { get; set; }
    }
}
