﻿using System.Collections.Generic;
using Database.Models;

namespace Infrastructure.DTO
{
    public class CountryDto
    { 
        public int CountryId { get; set; }
        public string Name { get; set; }
        public string Iso { get; set; }
        public virtual ICollection<Airport> Airports { get; set; }
    }
}
