﻿using Database.Models;

namespace Infrastructure.DTO
{
    public class PutUserDto
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public UserRole UserRole { get; set; }
    }
}
