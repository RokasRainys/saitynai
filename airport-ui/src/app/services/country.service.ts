import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import { Country } from '../models/country';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  private readonly countriesApi = `${environment.webApiUrl}/countries`;

  constructor(private http: HttpClient) { }

  getCountries(): Observable<Country[]> {
    return this.http.get<Country[]>(this.countriesApi);
  }
  deleteCountry(country: Country): Observable<Country> {
    return this.http.delete<Country>(`${this.countriesApi}/${country.countryId}`);
  }
}
