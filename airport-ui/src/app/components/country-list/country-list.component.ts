import { Component, OnInit } from '@angular/core';
import { Country } from '../../models/country';
import { CountryService } from '../../services/country.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.scss']
})
export class CountryListComponent implements OnInit {

  countries: Country[];
  errorMessage: string;

  constructor(private countryService: CountryService) { }

  ngOnInit() {
    this.countryService.getCountries().subscribe(countries => {
      this.countries = countries;
    },
    error => {
      console.error(error);
      this.errorMessage = error.message;
    });
  }

  onDeleteButtonClick(country: Country) {
    this.countryService.deleteCountry(country)
    .subscribe();
  }
}
