import { Airport } from './airport';

export class Country {
    countryId: number;
    name: string;
    iso: string;
    airports: Array<Airport>;
}
