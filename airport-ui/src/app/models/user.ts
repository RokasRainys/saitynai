export class User {
    id: number;
    username: string;
    role: UserRole;
    password: string;
}

export enum UserRole {
    RoleAdmin,
    RoleUser
}
