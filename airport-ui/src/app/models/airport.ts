import { Country } from './country';

export class Airport {
    id: number;
    name: string;
    country: Country;
}
