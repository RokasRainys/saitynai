﻿namespace Database.Models
{
    public enum UserRole
    {
        RoleAdmin,

        RoleUser,
    }
    public class User
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public UserRole UserRole { get; set; }
        public string Token { get; set; }
    }
}
