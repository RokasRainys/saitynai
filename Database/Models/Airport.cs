﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Database.Models
{
    public class Airport
    {
        public int AirportId { get; set; }
        public string Name { get; set; }
        [ForeignKey("CountryId")]
        public int CountryId { get; set; }
        public virtual Country Country { get; set; }
    }
}
