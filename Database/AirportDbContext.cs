﻿using Database.Models;
using Microsoft.EntityFrameworkCore;

namespace Database
{
    public class AirportDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Airport> Airports { get; set; }

        public AirportDbContext(DbContextOptions<AirportDbContext> options) : base(options)
        {
        }
    }
}
