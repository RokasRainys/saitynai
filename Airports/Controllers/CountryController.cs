﻿using System;
using System.Collections.Generic;
using Database.Models;
using Infrastructure.DTO;
using Infrastructure.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Airports.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CountryController : ControllerBase
    {
        private readonly IService<Country> _service;
        private readonly ICountryService _countryService;

        public CountryController(IService<Country> service, ICountryService countryService)
        {
            _service = service;
            _countryService = countryService;
        }

        // GET: api/Country
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_countryService.ReadAll());
            }
            catch (Exception)
            {
                return BadRequest(new {message = "Get request failed"});
            }
        }

        // GET: api/Country/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                return Ok(_countryService.Read(id));
            }
            catch (Exception)
            {
                return BadRequest(new { message = "Get request failed" });
            }
        }

        // POST: api/Country
        [HttpPost]
        public IActionResult Post([FromBody] CountryDto countryDto)
        {
            var country = new Country {Iso = countryDto.Iso, Name = countryDto.Name, Airports = countryDto.Airports};
            try
            {
                _service.Create(country);
            }
            catch (Exception)
            {
                return BadRequest(new {message = "Post request failed"});
            }

            return Ok();
        }

        // PUT: api/Country/
        [HttpPut]
        public IActionResult Put([FromBody] Country countryDto)
        {
            var country = new Country { Iso = countryDto.Iso, Name = countryDto.Name, Airports = countryDto.Airports };
            country.CountryId = countryDto.CountryId;
            //            try
            //            {
            _countryService.Update(country, countryDto.CountryId);
//            }
//            catch (Exception)
//            {
//                return BadRequest(new {message = "Put request failed"});
//            }

            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _service.Delete(id);
            }
            catch (Exception)
            {
                return BadRequest(new {message = "Delete request failed"});
            }

            return Ok();
        }
    }
}
