﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using Database.Models;
using Infrastructure.DTO;
using Infrastructure.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Airports.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AirportController : ControllerBase
    {
        private readonly IService<Airport> _service;
        private readonly IService<Country> _countryService;

        public AirportController(IService<Airport> service, IService<Country> countryService)
        {
            _service = service;
            _countryService = countryService;
        }

        // GET: api/Airport
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var airports = _service.ReadAll();
                foreach (var airport in airports)
                {
                    airport.Country = _countryService.Read(airport.CountryId);
                }
                return Ok(airports);
            }
            catch (Exception)
            {
                return BadRequest("Get was unsuccessful");
            }
        }

        // GET: api/Airport/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var airport = _service.Read(id);
                airport.Country = _countryService.Read(airport.CountryId);
                return Ok(airport);
            }
            catch (Exception)
            {
                return BadRequest(new {message = "Get request failed"});
            }
        }

        // POST: api/Airport
        [HttpPost]
        public IActionResult Post([FromBody] AirportDto airportDto)
        {
            var airport = new Airport
                {Name = airportDto.Name, Country = airportDto.Country, CountryId = airportDto.CountryId};
            try
            {
                airport.Country.Airports.Add(airport);
                _countryService.Update(airport.Country, airport.CountryId);
                _service.Create(airport);
            }
            catch (Exception)
            {
                return BadRequest(new { message = "Post was unsuccessful" });
            }

            return Ok();
        }

        // PUT: api/Airport
        [HttpPut]
        public IActionResult Put([FromBody] AirportDto airportDto)
        {
            var airport = new Airport
                { AirportId = airportDto.AirportId, Name = airportDto.Name, Country = airportDto.Country, CountryId = airportDto.CountryId };
            try
            {
            _service.Update(airport, airportDto.AirportId);
            }
            catch (Exception)
            {
                return BadRequest(new {message = "Put was unsuccessful"});
            }

            return Ok();

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _service.Delete(id);
            }
            catch (Exception)
            {
                return BadRequest(new {message = "Delete was unsuccessful"});
            }

            return Ok();
        }
    }
}
