﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Database.Models;
using Infrastructure.DTO;
using Infrastructure.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Airports.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly IService<User> _service;
        private readonly IUserService _userService;

        public UserController(IService<User> service, IUserService userService)
        {
            _service = service;
            _userService = userService;
        }

        private bool IsAuthorized()
        {
            return User.IsInRole(UserRole.RoleAdmin.ToString());
        }

        // GET: api/User
        [HttpGet]
        public IActionResult Get()
        {
            if (!IsAuthorized())
                return StatusCode(403);
            try
            {
                return Ok(_service.ReadAll());
            }
            catch (Exception)
            {
                return BadRequest(new {message = "Get request failed"});
            }
        }

        // GET: api/User/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            if (!IsAuthorized())
                return StatusCode(403);
            try
            {
                return Ok(_service.Read(id));
            }
            catch (Exception)
            {
                return BadRequest(new { message = "Get request failed" });
            }
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] UserDto user)
        {
            var result = _userService.AuthenticateUser(user);

            if (result == null) return BadRequest(new { message = "Username or password incorrect" });

            var userWithToken = _userService.GenerateJWT(result);

            return Ok(userWithToken);
        }

        // POST: api/User
        [HttpPost]
        public IActionResult Post([FromBody] UserDto userDto)
        {
            if (!IsAuthorized())
                return StatusCode(403);

            var user = new User();
            _userService.CreatePasswordHash(userDto.Password, out var passwordHash, out var passwordSalt);
            user.Username = userDto.Username;
            user.UserRole = UserRole.RoleUser;
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            try
            {
                _service.Create(user);
            }
            catch (Exception)
            {
                return BadRequest(new {message = "Post request failed"});
            }

            return Ok(new {message = "Success"});
        }
        // PUT: api/User
        [HttpPut]
        public IActionResult Put([FromBody] PutUserDto userDto)
        {
            if (!IsAuthorized())
                return StatusCode(403);

            var user = new User { Username = userDto.Username, UserRole = userDto.UserRole };
            user.UserId = userDto.UserId;
            _userService.CreatePasswordHash(userDto.Password, out var passwordHash, out var passwordSalt);
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            try
            {
                _service.Update(user, user.UserId);
            }
            catch (Exception)
            {
                return BadRequest(new {message = "Put request failed"});
            }
            return Ok();
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!IsAuthorized())
                return StatusCode(403);
            try
            {
                _service.Delete(id);
            }
            catch (Exception)
            {
                return BadRequest(new {message = "Delete request failed"});
            }

            return Ok();
        }
    }
}
